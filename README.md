# MWE utilities

Utility scripts for MWEs.

# Installation

Under virtual environment:

```
python setup.py install
```

Or, if you want a local installation:

```
python3 setup.py install --user
```

This will create the following scripts:

 - `crawl_wiktionary_locutions`: get french wiktionary locutions given their type;
 - `find_mwe_patterns`: find MWE patterns in dep-xml files using an MWE lexicon;
 - `count_mwes`: count MWE patterns that are in a CSV format;
 - `query_depxml`: query a depxml corpus with dependency patterns;
 - `query_cupt`: query a cupt corpus with dependency patterns;
 - `depxml_to_text`: transform a tar.gz with depxml files into a single text file (one sentence per line and tokens separated by a space);
 - `to_dpath`: transform queries into a perl script using dpath queries;
 - `to_smg`: transform a MWE dependency pattern list to dpath requests;
 - `to_treetagger`: transform depXML files into a single treetagger file.


When querying for MWEs, the first step is to create the list of queries from
examples. This is done using `find_mwe_patterns` to generate a raw list of 
patterns with repetitions, followed by `count_mwes` to count those pattern and
sort them by decreasing number. After that, `query_depxml` can be launched.

Those scripts can be piped, but it would be better to only do that for testing
purposes, as `find_mwe_patterns` is computationally heavy.

# Example

An example is given in the folder `./examples`. You can try to find patterns and
query them in a depxml corpus by launching the commands given in `commands.txt`:

```
find_mwe_patterns ./mwe-list.txt ./corpus.tar.gz | count_mwes --merge | query_depxml ./corpus.tar.gz
```

The command above will look into an already given MWE list and look for them in
the `.tar.gz` corpus, count them (and merge dependency patterns that have an
overlap in the MWEs they match to the most frequent pattern) and then, using
those dependency patterns, will query the corpus to look for more MWEs.

```
crawl_wiktionary_locutions verbales | find_mwe_patterns corpus.tar.gz | count_mwes --merge | query_depxml ./corpus.tar.gz
```

The command above will first look for verbal locutions in the french Wiktionary
and then proceed as the previous command.
