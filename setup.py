try:
    from setuptools import find_packages, setup
except ImportError:
    from distutils.core import find_packages, setup

setup(
    name="mweu",
    description="MWE utilities",
    long_description="MWE utilities is a set of tools and commands to have some basic processing"
                     " related to MWEs.",
    version="1.0.0",
    author_email="yoann.dupont@inria.fr",
    maintainer="Yoann Dupont",
    maintainer_email="yoann.dupont@inria.fr",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'count_mwes=mweu.count_mwes:parse_cl',
            'find_mwe_patterns=mweu.find_mwe_patterns:parse_cl',
            'query_depxml=mweu.query_depxml:parse_cl',
            'query_cupt=mweu.query_cupt:parse_cl',
            'depxml_to_text=mweu.depxml_to_text:parse_cl',
            'to_dpath=mweu.to_dpath:parse_cl',
            'to_smg=mweu.to_smg:parse_cl',
            'to_treetagger=mweu.to_treetagger:parse_cl',
            'to_mwetk_candidates=mweu.to_mwetk_candidates:parse_cl',
            'crawl_wiktionary_locutions=mweu.crawl_wiktionary_locutions:parse_cl'
        ]
    },
    classifiers=[
        'Programming Language :: Python :: 3'
        'Programming Language :: Python :: 3.5'
    ],
    zip_safe=True
)
