"""Transform a MWE dependency pattern list to dpath requests.
"""

import io
import sys


def to_tree1(tokens):
    """Transform tokens to a tree structure than can be parsed more easily."""
    def add_indices(tokens):
        lst = []
        i = 0
        for token in tokens:
            if token[0] not in "()":
                lst.append("{token}@{i}".format(token=token, i=i))
                i += 1
            else:
                lst.append(token)
        return lst

    stack = []
    toks = add_indices(tokens)[::-1]
    val = toks.pop()
    d = {"({val},)".format(val=val): {}}
    tmp = list(d.values())[0]
    path = [list(d.keys())[0]]
    for i in range(len(toks)):
        item = toks.pop()
        if item.startswith('('):
            item = item[1:]
        stack.append(item)
        if '@' in item:
            cat = stack.pop()
            edge = stack.pop()
            key = "({cat},{edge})".format(cat=cat, edge=edge)
            tmp[key] = {}
            tmp = tmp[key]
            path.append(key)
        elif ')' == item:
            path.pop()
            tmp = d
            for node in path:
                tmp = tmp[node]
    return d


def put(dct, context_value, out_fd=sys.stdout):
    """Put the dpath query in out_fd."""
    def put_rec(subd):
        if len(subd.keys()) > 1:
            for key in sorted(subd.keys(), key=lambda x: int(x[x.index("@")+1: x.index(',')])):
                cat = key[1: key.index('@')]
                edge = key[key.index(',')+1: -1]
                edge = edge.replace(context_value, "")
                out_fd.write(' .(all_out is_{edge} target is_{cat}'.format(edge=edge, cat=cat))
                put_rec(subd[key])
                out_fd.write(')')
        elif len(subd.keys()) == 1:
            key = list(subd.keys())[0]
            cat = key[1: key.index('@')]
            edge = key[key.index(',')+1: -1]
            edge = edge.replace(context_value, "")
            out_fd.write(" all_out is_{edge} target is_{cat}".format(edge=edge, cat=cat))
            put_rec(subd[key])

    key = list(dct.keys())[0]
    cat = key[1: key.index('@')]
    out_fd.write("dpath is_{cat}".format(cat=cat))
    put_rec(dct[key])


def put_code(dct, var2id, context_value, context, out_fd=sys.stdout):
    """Output the query-related code (dpath + variables)"""
    methods = {"edge": set(), "cat": set()}

    def put_code_rec(subd, var_name, nth):
        if len(subd.keys()) > 1:
            for key in sorted(subd.keys(), key=lambda x: int(x[x.index("@")+1: x.index(',')])):
                cat = key[1: key.index('@')]
                edge = key[key.index(',')+1: -1]
                if context_value not in cat:
                    methods["cat"].add(cat)
                if context_value not in edge:
                    methods["edge"].add(edge)
                if context_value in edge:
                    edge = edge[: edge.index(context_value)]
                    context.add("$var_{nth}".format(nth=nth+1))
                var2id["$var_{nth}".format(nth=nth+1)] = key[key.index('@'): key.index(',')]
                dpath = (
                    "        my $var_{nth} = ({var_name}->apply(dpath all_out is_{edge} "
                    "target is_{cat}))[0];\n"
                ).format(nth=nth+1, var_name=var_name, edge=edge, cat=cat)
                out_fd.write(dpath)
                out_fd.write("        if (not $var_{nth}) {{ last; }}\n".format(nth=nth+1))
                nth = put_code_rec(subd[key], "$var_{nth}".format(nth=nth+1), nth+1)
        elif len(subd.keys()) == 1:
            key = list(subd.keys())[0]
            cat = key[1: key.index('@')]
            edge = key[key.index(',')+1: -1]
            if context_value not in cat:
                methods["cat"].add(cat)
            if context_value not in edge:
                methods["edge"].add(edge)
            if context_value in edge:
                edge = edge[: edge.index(context_value)]
                context.add("$var_{nth}".format(nth=nth+1))
            var2id["$var_{nth}".format(nth=nth+1)] = key[key.index('@'): key.index(',')]
            dpath = (
                "        my $var_{nth} = ({var_name}->apply("
                "dpath all_out is_{edge} target is_{cat}))[0];\n"
            ).format(nth=nth+1, var_name=var_name, edge=edge, cat=cat)
            out_fd.write(dpath)
            out_fd.write("        if (not $var_{nth}) {{ last; }}\n".format(nth=nth+1))
            nth = put_code_rec(subd[key], "$var_{nth}".format(nth=nth+1), nth+1)
        return nth

    key = list(dct.keys())[0]
    put_code_rec(dct[key], "$node", nth=0)
    return methods


def main(inputfilename, outputfilename, column=2, context_value="::context"):
    with open(inputfilename, "r", encoding="utf-8") as input_stream, \
            open(outputfilename, "w", encoding="utf-8") as output_stream:
        string_stream = io.StringIO()
        methods = {"edge": set(), "cat": set()}

        output_stream.write(
            "sub scrape_empties {\n"
            "    my @result = ();\n"
            "    foreach(@_) {\n"
            "        if ($_) {\n"
            "            push @result, $_;\n"
            "        }\n"
            "    }\n"
            "    return @result;\n"
            "}\n\n"
        )
        string_stream.write(
            "sub collect {\n"
            "    my ($io,$data,$seg,$sid) = @_;\n"
            "\n"
            "    my $cas = DepXML->new();\n"
            "    eval {\n"
            "        $cas->parse($io);\n"
            "    };\n"
            "    if ($@) {\n"
            '        print STDERR "**pb ${seg}.E${sid}: $@\\n";\n'
            "        $cas->reset;\n"
            "        return;\n"
            "    }\n"
            "    $seg ||= 'dummy';\n"
            "    $sid ||= 1;\n"
            "    my @keep = ();\n"
            "\n"
            '    # print STDERR "Process ${seg}.E$sid\\n";#1. (mwe v (cll cll ) )\n'
            "    foreach my $node ($cas->find_nodes(dpath is_v all_out is_cll target is_cll)) {\n"
            "        my $var_1 = ($node->apply(dpath all_out is_cll target is_cll))[0];\n"
            "        my @toks = scrape_empties($node, $var_1);\n"
            "        push(\n"
            "            @{$data->{mwe}},\n"
            "            join(\n"
            '                "\t",\n'
            '                "$seg.E$sid",\n'
            "                'ncpred1',\n"
            "                form(@toks),\n"
            "                lemma(@toks),\n"
            "                fids(@toks)\n"
            "            )\n"
            "        );\n"
            "    }\n\n"
        )
        nth = 0
        for line in input_stream:
            nth += 1
            try:
                tree_dep = line.strip().split('\t')[column]
                tokens = tree_dep.split()
                tokens = tokens[1:-1]
                string_stream.write("    #{nth}. {tree_dep}\n".format(nth=nth, tree_dep=tree_dep))
                string_stream.write("    foreach my $node ($cas->find_nodes(")
                put(to_tree1(tokens), context_value, out_fd=string_stream)
                string_stream.write(" )) {\n")
                var2id = {}
                context = set()
                methods = {
                    key: methods[key] | value
                    for key, value in put_code(
                        to_tree1(tokens),
                        out_fd=string_stream,
                        var2id=var2id,
                        context_value=context_value,
                        context=context
                    ).items()
                }
                string_stream.write(
                    "        my @toks = scrape_empties($node, {});\n"
                    .format(", ".join(item for item in sorted(
                        [var for var in var2id.keys() if var not in context],
                        key=lambda x: var2id[x]
                    )))
                )
                string_stream.write(
                    "        @toks = sort {(split /F/, @{$a->fids}[0])[1] "
                    "<=> (split /F/, @{$b->fids}[0])[1]} @toks; "
                    "# sorting by indices in sentence\n"
                )
                string_stream.write(
                    "        push(\n"
                    "            @{$data->{mwe}},\n"
                    "            join(\n"
                    '                "\\t",\n'
                    '                "$seg.E$sid",\n'
                    "                'ncpred{}',\n"
                    "                form(@toks),\n"
                    "                lemma(@toks),\n"
                    "                fids(@toks)\n"
                    "            )\n"
                    "        );\n"
                    "    }\n\n".format(nth)
                )
            except IndexError as ie:
                print(
                    "cannot process ({ie}): {tree_dep}".format(ie=ie, tree_dep=tree_dep),
                    file=sys.stderr
                )
        string_stream.write("}")

        for cat in methods["cat"]:
            output_stream.write(
                "sub DepXML::Node::is_{cat} {{\n"
                "    return $_[0]->cat eq '{cat}'\n"
                "}}\n"
                "\n".format(cat=cat)
            )
        for edge in methods["edge"]:
            output_stream.write(
                "sub DepXML::Edge::is_{edge} {{\n"
                "  return $_[0]->label eq '{edge}'\n"
                "}}\n"
                "\n".format(edge=edge)
            )
        output_stream.write(string_stream.getvalue())


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument("inputfilename", help="The path to the input file.")
    parser.add_argument("outputfilename", help="The path to the output file.")
    parser.add_argument(
        "-c", "--column",
        type=int,
        default=2,
        help="The path to the output file."
    )
    parser.add_argument(
        "--context-value",
        default="::context",
        help="The context marquer (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
