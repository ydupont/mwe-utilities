def flatten(trie):
    """Return a list from trie using a breadth-first search. Order of elements is not garanteed.

    Leaves in trie should not be a string, but an empty dict:
    - valid : {"foo": {"bar": {}}}
    - invalid : {"foo": "bar"}
    """
    def flatten_rec(structure, acc):
        vals = []
        for key, val in structure.items():
            acc.append(key)
            vals.append(val)
        for val in vals:
            flatten_rec(val, acc)
        return acc

    return flatten_rec(trie, [])
