# coding:utf-8

"""Crawl french locutions from the Wiktionary given their Part-Of-Speech.
"""

import urllib.parse
import time
import sys
from bs4 import BeautifulSoup
import requests


def get_name(locution_kind):
    """Map locution_kind to an appropriate Wiktionary category."""

    if locution_kind == "adjectives":
        return "adjectivales"
    elif locution_kind == "prépositionnelles":
        return "prépositives"
    else:
        return locution_kind


def get_url(locution_kind):
    """Get the url where all locutions of the locution_kind category are listed."""

    lk = urllib.parse.quote(locution_kind)
    if "locution" in locution_kind.lower():
        return \
            "https://fr.wiktionary.org/w/index.php" \
            "?title=Cat%C3%A9gorie:{}_en_fran%C3%A7ais&from=#mw-pages".format(lk.replace(" ", "_"))
    else:
        return \
            "https://fr.wiktionary.org/w/index.php" \
            "?title=Cat%C3%A9gorie:Locutions_{}_en_fran%C3%A7ais&from=#mw-pages".format(lk)


def main(locution_kind, page_limit=0, output_file=sys.stdout):
    """Crawl Wiktionary locutions of the locution_kind category."""

    n_pages = 0
    locution_name = get_name(locution_kind)
    limit = (page_limit if page_limit >= 0 else 0)

    print("using locution name: {}".format(locution_name), file=sys.stderr)

    next_page = get_url(locution_name)
    locutions = []
    while next_page:
        response = requests.get(next_page)
        content = response.text
        n_pages += 1
        html = BeautifulSoup(content, "html.parser")
        next_page = None
        for node in html.find_all('a'):
            if node.text == "page suivante":
                next_page = node["href"]

        if next_page:
            next_page = "https://fr.wiktionary.org" + next_page

        for node in html.find_all('div', attrs={'class': 'mw-category-group'}):
            for a_node in node.find_all('a'):
                locutions.append(a_node.text)

        print("processed {n} page(s)".format(n=n_pages), file=sys.stderr)

        if limit > 0:
            limit -= 1
            if limit == 0:
                break

        time.sleep(0.5)  # maybe not even needed

    for loc in locutions:
        loc = loc.replace("’", "' ")
        output_file.write(loc + "\n")


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(
        description=__doc__.split("\n")[0]
    )
    parser.add_argument(
        "locution_kind",
        choices=(
            "adjectives",
            "adjectivales",
            "adverbiales",
            "conjonctives",
            "déterminatives",
            "interjectives",
            "nominales",
            "postpositives",
            "prépositives",
            "prépositionnelles",
            "pronominales",
            "verbales",
            "Locutions-phrases"
        ),
        help="the kind of locution to crawl from wiktionary"
    )
    parser.add_argument(
        "output_file",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "--page-limit",
        type=int, default=0,
        help="The maximum number of pages to process (default: %(default)s)."
    )

    args = parser.parse_args(argv)
    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
