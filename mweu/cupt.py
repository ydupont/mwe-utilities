# -*- coding: utf-8 -*-
"""Python objects and methods to handle depxml files.
"""

import collections
import sys


class Token:
    """A token in a cupt schema. We take every information in a cupt token.

    ID	FORM	LEMMA	UPOS	XPOS	FEATS	HEAD	DEPREL	DEPS	MISC	PARSEME:MWE
    4	apprend	apprendre	VERB	_	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	0	root	_	_	*
    """

    def __init__(
        self, id, form, lemma, upos, xpos, feats, head, deprel, deps, misc, mwe,
        *args, **kwargs
    ):
        self.id = id
        self.form = form
        self.lemma = lemma
        self.upos = upos
        self.xpos = xpos
        self.feats = feats
        if self.feats != "_":
            self.features = {
                key: value
                for key, value in [
                    item.split("=", 1) for item in self.feats.split("|")
                ]
            }
        else:
            self.features = {}
        self.head = head
        self.deprel = deprel
        self.deps = deps
        self.misc = misc
        self.mwe = mwe
        self.incoming = collections.defaultdict(list)
        self.outgoing = collections.defaultdict(list)

    def __str__(self):
        return "\t".join([
            self.id,
            self.form,
            self.lemma,
            self.upos,
            self.xpos,
            self.feats,
            self.head,
            self.deprel,
            self.deps,
            self.misc,
            self.mwe,
        ])

    @property
    def cat(self):
        """Return the category of the token (UPOS).
        This allows to interface nicely with mweu.dpath.Query
        """
        return self.upos


class Edge:
    """Just an imperfect copy of mweu.depxml.Edge to use mweu.find_mwe_patterns.output_pattern_tree

    This object is WET wrt Token and Sentence objects, mweu.depxml.Sentence should be changed
    to remove this object.
    """

    def __init__(self, source, target, label):
        self.source = source
        self.target = target
        self.label = label


class Sentence:
    """A sentence in a cupt schema."""

    def __init__(self, headers, nodes):
        self.headers = headers
        self.nodes = nodes
        self.root = [node for node in self.nodes if node.deprel == "root"][0]
        self.id2node = {node.id: node for node in self.nodes}
        self.nodes2edge = {}  # TODO: remove, this field is WET ==> Modify mweu.depxml.Sentence
        for token in self.nodes:
            if token.head not in ("_", "0"):  # contractions and roots
                self.id2node[token.head].outgoing[token.deprel].append(self.id2node[token.id])
                self.id2node[token.id].incoming[token.deprel].append(self.id2node[token.head])
                self.nodes2edge[(self.id2node[token.head], self.id2node[token.id])] = Edge(
                    self.id2node[token.head],
                    self.id2node[token.id],
                    token.deprel
                )

    def __str__(self):
        return "# source_sent_id = {}\n".format(self.source_sent_id) \
                + "# text = {}\n".format(self.text) \
                + "\n".join(str(token) for token in self.nodes)

    def __getitem__(self, key):
        return self.nodes[key]

    @property
    def source_sent_id(self):
        return self.headers["source_sent_id"]

    @property
    def text(self):
        return self.headers["text"]

    def edge(self, source, target):
        """Return the label of the edge linking source to target."""
        return self.nodes2edge[source, target].label


def read_file(file_name):
    """Read file_name and return a list of cupt.Sentence.

    file_name is a cupt file http://multiword.sourceforge.net/cupt-format/
    """
    def get_sentence(contents):
        """Return a Sentence from the list.

        contents is just a token line in a cupt file split into a list.
        """
        header_lines = [content for content in contents if content.startswith("#")]
        headers = {
            key: value
            for key, value in [
                (
                    line[1:].strip().split(" = ", 1)
                    if " = " in line  # "# text = [...]"
                    else line[1:].strip().split(": ", 1)  # "# duplicate: [...]"
                )
                for line in header_lines
            ]
        }
        tokens = contents[len(header_lines):]
        nodes = []
        try:
            for token in tokens:
                nodes.append(Token(*token.split("\t")))
        except Exception:
            print(token, file=sys.stderr)
            raise
        return Sentence(headers, nodes)
    sentences = []
    with open(file_name) as input_stream:
        contents = []
        next(input_stream)  # first line is the set of columns
        for line in input_stream:
            if not line.strip():
                if contents:
                    sentences.append(get_sentence(contents))
                    del contents[:]
            else:
                contents.append(line.strip())
        if contents:
            sentences.append(get_sentence(contents))
            del contents[:]
    return sentences
