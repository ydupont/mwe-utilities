# -*- coding: utf-8 -*-

"""Find MWE patterns in dep-xml files using an MWE lexicon.
"""

import collections
import itertools
import multiprocessing
import io
import sys
import time
import mweu.depxml as depxml
import mweu.utils


def all_cursors(d):
    """Retun key/subtree pairs for every element in d."""

    def cursors_rec(dct, acc):
        if dct:
            for key, value in dct.items():
                acc = cursors_rec(value, acc)
                acc.append((key, value))
        return acc
    return cursors_rec(d, [])


def leaf_cursors(d):
    """Retun key/subtree pairs for leaf elements in d."""

    def leaf_cursors_rec(dct, acc):
        if dct:
            for key, value in dct.items():
                acc = leaf_cursors_rec(value, acc)
                if not value:
                    acc.append((key, value))
        return acc
    return leaf_cursors_rec(d, [])


def check_tokens(reference, comparison):
    """Check that all tokens in reference are in comparison"""
    for token in comparison:
        if token.lemma in reference:
            reference -= collections.Counter([token.lemma])
        elif token.form in reference:
            reference -= collections.Counter([token.form])
    return len(reference) == 0


def output_pattern_tree(
    tree,
    sentence,
    depth=0,
    source=None,
    context_nodes=None,
    extra_information="",
    output_stream=sys.stdout
):
    """Print the pattern (subtree) using PTB tree representation."""
    for key in sorted(tree.keys(), key=lambda x: int(x.id.split("n")[-1])):
        extra_tree = (
            '|tree=' + key.tree
            if "tree" in extra_information
            else ''
        )
        extra_features = (
            '|'.join("{}={}".format(k, v) for k, v in key.features.items())
            if "features" in extra_information
            else ''
        )
        extra_features = ('|' if extra_features else '') + extra_features
        context = ('|context=+' if context_nodes and key in context_nodes else '')
        if source:
            output_stream.write('({} {}{}{}{} '.format(
                sentence.edge(source, key),
                key.cat,
                extra_tree,
                extra_features,
                context
            ))
        else:
            output_stream.write('(mwe {}{}{}{} '.format(
                key.cat,
                extra_tree,
                extra_features,
                context
            ))
        output_pattern_tree(
            tree[key],
            sentence,
            depth+1,
            key,
            context_nodes=context_nodes,
            extra_information=extra_information,
            output_stream=output_stream
        )
        output_stream.write(') ')


def process(args):
    """Look for MWE entries in a lexicon in a sentence and output its dependency pattern.
    args is a tuple containing all the arguments. It allows to pass the method
    to the multiprocessing.Pool.map method."""

    sentence, token2mwe, root_category, context, extra_information = args

    # creating minimal MWE candidates based on tokens

    allowed_elements = set(itertools.chain.from_iterable(
        (node.form, node.lemma) for node in sentence.nodes
    ))
    sub_token2mwe = {
        key: set(val for val in value if (set(val) & allowed_elements) == set(val))
        for key, value in token2mwe.items()
    }
    sub_token2mwe = {key: value for key, value in sub_token2mwe.items() if value}
    mwe_set = set(itertools.chain.from_iterable(sub_token2mwe.values()))

    # looking for MWEs in sentence
    with io.StringIO() as output_stream:
        for mwe_tuple in mwe_set:
            mwe = set(mwe_tuple)
            pathes = depxml.find_all(sentence, mwe, root_category=root_category)
            for root, missing in [(rt, mssg) for rt, mssg in pathes.roots.items() if not mssg]:
                tree = depxml.clean(pathes.pathes1[root])
                tree = {root: tree}
                basetree = tree
                # keeping the base tree allows to compare its variations without having to
                # recompute the base tree from a more complex structure
                ctx = set()
                if context:
                    cursors = all_cursors(tree)
                    try:
                        incoming = list(root.incoming.values())[0][0]
                        # incoming has one key/val pair which is a list with only one value
                        tree = {incoming: tree}
                        ctx.add(incoming)
                    except IndexError:
                        pass
                    for node, subtree in cursors:
                        for label in node.outgoing:
                            if not node.outgoing[label][0].form:
                                continue
                            if node.outgoing[label][0] not in subtree:
                                subtree[node.outgoing[label][0]] = {}
                                ctx.add(node.outgoing[label][0])
                tokens = [
                    token
                    for token in mweu.utils.flatten(tree)
                    if token.form
                ]
                basetokens = [
                    token
                    for token in mweu.utils.flatten(basetree)
                    if token.form
                ]
                if not check_tokens(collections.Counter(mwe_tuple), tokens):
                    continue
                tokens.sort(key=lambda x: [int(i) for i in x.cluster.split("_")[1:]])
                output_stream.write(sentence.name + '\t')
                output_stream.write(" ".join(" ".join(token.fids) for token in basetokens) + '\t')
                output_stream.write(" ".join(mwe_tuple) + '\t')
                output_pattern_tree(
                    basetree,
                    sentence,
                    output_stream=output_stream
                )
                output_stream.write("\t")
                output_stream.write(" ".join(" ".join(token.fids) for token in tokens) + '\t')
                output_stream.write(" ".join(token.form for token in tokens) + '\t')
                output_pattern_tree(
                    tree,
                    sentence,
                    context_nodes=ctx,
                    extra_information=extra_information,
                    output_stream=output_stream
                )
                output_stream.write("\n")
        result = output_stream.getvalue()
    return result[:]


def main(
    lexicon,
    infiles,
    root_category=None,
    context=False,
    output_file=sys.stdout,
    number_of_processors=1,
    extra_information=[],
):
    stop_list = set((
        'un', 'une', 'à', 'le', 'la', 'les', "l'", 'son', 'sa', 'ses', "s'",
        'de', 'du', 'des', "d'"
    ))
    token2mwe = collections.defaultdict(set)
    for line in lexicon:
        tokens = tuple(line.strip().split())
        for token in [t for t in tokens if t not in stop_list]:
            token2mwe[token].add(tokens)

    pool = (
        multiprocessing.Pool(processes=number_of_processors)
        if number_of_processors > 1
        else None
    )
    total_time = 0
    for inputfilename in infiles:
        start = time.time()
        print("processing {}...".format(inputfilename), end=" ", file=sys.stderr)
        sentences = depxml.read_targz(inputfilename)
        input_list = list(zip(
            sentences,
            itertools.cycle([token2mwe]),
            itertools.cycle([root_category]),
            itertools.cycle([context]),
            itertools.cycle([extra_information])
        ))
        results = (
            pool.map(process, input_list)
            if pool is not None
            else map(process, input_list)
        )
        for result in results:
            output_file.write(result)
        output_file.flush()  # does not write otherwise?!?!
        laps = time.time() - start
        total_time += laps
        print("{} / {}".format(laps, total_time), file=sys.stderr)
        sys.stderr.flush()


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(
        description=__doc__.split("\n")[0]
    )
    parser.add_argument(
        "lexicon",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The lexicon of MWE lemmas to look for."
    )
    parser.add_argument(
        "infiles",
        nargs="+",
        help="The files where to look for MWEs."
    )
    parser.add_argument(
        "-r", "--root-category",
        help="The root category. If unset, takes all categories"
    )
    parser.add_argument(
        "-c", "--context",
        action="store_true",
        help="Output context in the dependency tree."
    )
    parser.add_argument(
        "-o", "--output-file",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "-p", "--number-of-processors",
        type=int,
        default=1,
        help="The number of processors to use."
    )
    parser.add_argument(
        "-i", "--extra-information",
        default="None", choices=("None", "tree", "features", "tree+features"),
        help="add extra information in patterns."
    )

    args = parser.parse_args(argv)
    args.extra_information = (
        []
        if args.extra_information == "None"
        else args.extra_information.split("+")
    )
    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
