"""Transform a MWE dependency pattern list to dpath requests.
"""

import sys


cat2class = {
    # frmg
    "adj": "adjective",
    "adjPref": "adjective",
    "adv": "adverb",
    "advPref": "adverb",
    'advneg': 'adverb',
    "aux": "auxilliary",
    'clg': 'clitic',
    'cll': 'clitic',
    'clneg': 'clitic',
    "cla": "clitic",
    "cld": "clitic",
    "cln": "clitic",
    "clr": "clitic",
    'csu': 'clitic',
    'ilimp': 'clitic',
    "coo": "conjuction",
    'det': 'det',
    "N": "noun",
    'n': 'noun',
    'nc': 'noun',
    'ncpred': 'noun',
    "np": "noun",
    'N2': 'noun',
    'v': 'verb',
    'VMod': 'verb',
    'prep': 'prep',
    "predet": "pronoun",
    "prel": "pronoun",
    "pres": "pronoun",
    "pri": "pronoun",
    "pro": "pronoun",

    "CS": "?",
    "Infl": "?",
    "S": "?",
    "_": "?",
    "ce": "?",
    "comp": "?",
    "incise": "?",
    "number": "?",
    "que": "?",
    "que_restr": "?",
    "strace": "?",
    "supermod": "?",
    "unknown": "?",
    "xpro": "?",

    # PARSEME
    "ADJ": "adjective",
    "ADV": "adverb",
    "AUX": "auxilliary",
    "CCONJ": "conjunction",
    "DET": "det",
    "NOUN": "noun",
    "PRON": "pronoun",
    "PROPN": "noun",
    "VERB": "verb",

    "NUM": "cardinal?!",
    "SCONJ": "?!",
    "ADP": "adposition?!",
    "PART": "?!",
}


def to_tree(tokens):
    """Transform tokens to a tree structure than can be parsed more easily."""

    def add_indices(tokens):
        lst = []
        i = 0
        node2tree = {}
        for token in tokens:
            if token[0] not in "()":
                node = "{token}@{i}".format(token=token, i=i)
                lst.append(node)
                node2tree[node] = "T_{token}_{i}".format(token=token, i=i)
                i += 1
            else:
                lst.append(token)
        return lst, node2tree

    stack = []
    toks, node2tree = add_indices(tokens)
    toks = toks[::-1]
    val = toks.pop()
    d = {"({val},)".format(val=val): {}}
    tmp = list(d.values())[0]
    path = [list(d.keys())[0]]
    for i in range(len(toks)):
        item = toks.pop()
        if item.startswith('('):
            item = item[1:]
        stack.append(item)
        if '@' in item:
            cat = stack.pop()
            edge = stack.pop()
            key = "({cat},{edge})".format(cat=cat, edge=edge)
            tmp[key] = {}
            tmp = tmp[key]
            path.append(key)
        elif ')' == item:
            path.pop()
            tmp = d
            for node in path:
                tmp = tmp[node]
    return d, node2tree


def put(dct, node2tree, context_value, class_name, nth, tree_dep, out_fd=sys.stdout):
    """Put the dpath query in out_fd."""

    def put_rec(subd, from_key):
        root_node = from_key[1: from_key.index(',')]
        for key in sorted(subd.keys(), key=lambda x: int(x[x.index("@")+1: x.index(',')])):
            node = key[1: key.index(',')]
            edge = key[key.index(',')+1: -1]
            edge = edge.replace(context_value, "")
            out_fd.write("    subst({top}.{edge}) = {bot};\n".format(
                top=node2tree[root_node],
                edge=edge,
                bot=node2tree[node]
            ))
            put_rec(subd[key], key)

    key = list(dct.keys())[0]
    out_fd.write("class {}_{} {{\n".format(class_name, nth))
    out_fd.write("    % {nth}. {tree_dep}\n".format(nth=nth, tree_dep=tree_dep))
    for node, tree in sorted(node2tree.items(), key=lambda x: int(x[0].split("@")[1])):
        try:
            out_fd.write(
                "    {tree} <: {cls};\n".format(tree=tree, cls=cat2class[node[:node.index('@')]])
            )
        except KeyError:
            out_fd.write(
                "    {tree} <: {cls};\n"
                .format(tree=tree, cls=cat2class[node[:node.index('@')].split('|')[0]])
            )
    put_rec(dct[key], key)
    out_fd.write("}")


def main(inputfilename, outputfilename, column=2, context_value="::context", class_name="mwe"):
    with open(inputfilename, "r", encoding="utf-8") as input_stream, \
            open(outputfilename, "w", encoding="utf-8") as output_stream:
        nth = 0
        for line in input_stream:
            nth += 1
            try:
                tree_dep = line.strip().split('\t')[column]
                if "  )" in tree_dep:  # empty value
                    raise ValueError("empty node in tree")
                tokens = tree_dep.split()
                tokens = tokens[1:-1]
                # TODO: remove following line and handle properly properties
                tokens = [item.split('|')[0] for item in tokens]
                dct, node2tree = to_tree(tokens)
                put(dct, node2tree, context_value, class_name, nth, tree_dep, out_fd=output_stream)
                output_stream.write("\n\n")
            except (IndexError, ValueError) as err:
                print(
                    "cannot process ({err}): {tree_dep}".format(err=err, tree_dep=tree_dep),
                    file=sys.stderr
                )


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument("inputfilename", help="The path to the input file.")
    parser.add_argument("outputfilename", help="The path to the output file.")
    parser.add_argument(
        "-c", "--column",
        type=int,
        default=2,
        help="The column to look for trees in the input file (default: %(default)s)."
    )
    parser.add_argument(
        "--context-value",
        default="::context",
        help="The context marquer (default: %(default)s)"
    )
    parser.add_argument(
        "--class-name",
        default="mwe",
        help="The name to use for classes (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
