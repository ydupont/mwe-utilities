"""Transform a list of depxml files (stored in tar.gz) into a single text file that will be one
sentence per line and tokens separated by a space.
"""

import multiprocessing
import sys
import tarfile
import itertools
import lxml.etree as ET
import mweu.depxml as depxml


def process(clusters):
    forms = list(set(itertools.chain.from_iterable(c.forms for c in clusters)))
    try:
        forms.sort(key=lambda x: int(x.split('|', 1)[0].split('F')[1]))
    except IndexError:
        print(forms, file=sys.stderr)
        raise
    return [f.split('|', 1)[1] for f in forms]


def main(
    inputfilenames,
    number_of_processors=1
):
    pool = (
        multiprocessing.Pool(processes=number_of_processors)
        if number_of_processors > 1
        else None
    )
    for inputfilename in inputfilenames:
        sentences = []
        with tarfile.open(inputfilename, "r:gz") as tar:
            members = [m for m in tar.getmembers() if m.name.endswith(".dis.dep.xml")]
            for member in members:
                fd = tar.extractfile(member)
                try:
                    tree = ET.fromstring(fd.read())
                    sentences.append(sorted(
                        [depxml.Cluster(**cluster.attrib) for cluster in tree.iterfind("cluster")],
                        key=lambda x: (x.left, -x.right)
                    ))
                except ET.XMLSyntaxError:
                    pass
                except ValueError:
                    print("cannot process {}".format(member.name), file=sys.stderr)
                    raise
                if member.name == "frwikipedia_059/1/8/3/frwikipedia_059.E18354.dis.dep.xml":
                    print([t.forms for t in sentences[-1]], file=sys.stderr)
                fd.close()

        print("processing {}".format(inputfilename), file=sys.stderr)
        if pool:
            results = pool.map(process, sentences)
        else:
            results = [process(sentence) for sentence in sentences]

        for res in results:
            print(" ".join(res).strip().replace("'", "' "))


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.replace("\n", " "))
    parser.add_argument(
        "inputfilenames",
        nargs="+",
        help="The path to the input files (tar.gz files with .dis.dep.xml files)."
    )
    parser.add_argument(
        "-n", "--number-of-processors",
        type=int,
        default=1,
        help="The number of processors to use (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
