"""Count MWE patterns that are in a CSV format.

The dependency patterns use the PTB format and look like a constituency tree.
The script also outputs the cumulative percentage of the pattern's coverage
according to the number of encountered patterns in the input file.
"""

import collections
import argparse
import sys


def main(input_file, output_file, lemma_column=-2, pattern_column=-1, merge=True):
    count = collections.Counter()
    pattern2lemma = collections.defaultdict(set)

    if merge:
        lemma2pattern_count = collections.defaultdict(collections.Counter)
        for line in input_file:
            parts = line.strip().split('\t')
            lemma = parts[lemma_column]
            pattern = parts[pattern_column]
            count[pattern] += 1
            lemma2pattern_count[lemma][pattern] += 1
        for lemma, counter in lemma2pattern_count.items():
            items = counter.most_common()
            for pattern, pattern_count in items[1:]:
                counter[items[0][0]] += pattern_count
                count[items[0][0]] += pattern_count
                count[pattern] -= pattern_count
            pattern2lemma[items[0][0]].add(lemma)
        count = {key: value for key, value in count.items() if value > 0}
    else:
        for line in input_file:
            parts = line.strip().split('\t')
            lemma = parts[lemma_column]
            pattern = parts[pattern_column]
            count[pattern] += 1
            pattern2lemma[pattern].add(lemma)

    total = sum(count.values())
    cumul = 0
    for pattern, count in sorted(count.items(), key=lambda x: (-x[1], x)):
        cumul += count
        percent = 100 * count / total
        percent_cumul = 100 * cumul / total
        output_file.write(
            '{count}\t{percent:0.3f}\t{percent_cumul:0.3f}\t{pattern}\t'
            .format(count=count, percent=percent, percent_cumul=percent_cumul, pattern=pattern)
        )
        output_file.write(" / ".join(sorted(pattern2lemma[pattern])))
        output_file.write('\n')


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_file",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input file."
    )
    parser.add_argument(
        "output_file",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "--lemma-column",
        type=int, default=-2,
        help="Column for lemma information (default: %(default)s)"
    )
    parser.add_argument(
        "--pattern-column",
        type=int, default=-1,
        help="Column for pattern information (default: %(default)s)"
    )
    parser.add_argument(
        "--merge",
        action="store_true",
        help="Merge MWEs that have the same lemma to the most frequent pattern."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
