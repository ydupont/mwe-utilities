# -*- coding: utf-8 -*-
"""Transform queries into a perl script using dpath queries.
"""

import copy
import itertools
import functools


class Query:
    """A dpath query for finding subgraphs in a dependency graph."""

    def __init__(self, cat=None, edge=None, trie=None, leaf=None):
        if trie:
            if not leaf:
                raise ValueError("leaf cannot be None when giving trie.")
            self.trie = trie
            self.current = leaf
        else:
            if cat and edge:
                self.trie = {edge: {cat: {}}}
                self.current = self.trie[edge][cat]
            elif cat:
                self.trie = {cat: {}}
                self.current = self.trie[cat]
            elif edge:
                self.trie = {edge: {'': {}}}
                self.current = self.trie[edge]
            else:
                self.trie = {}
                self.current = self.trie

    def __len__(self):
        return self._length(True)

    def _length(self, do_count):
        n = 0
        try:
            for key, val in self.trie.items():
                if key != "_context":
                    n += do_count + Query(trie=val, leaf="_")._length(not do_count)
                elif key == "_context" and val == "-":
                    n -= 1
        except AttributeError:
            return 0
        return n

    def all_out(self, edge, cat, is_context=False):
        """Look for all nodes that match the dependency arc with label edge to a node
        with category cat.
        """
        self.current[edge] = {cat: {}}
        if is_context:
            self.current[edge]["_context"] = is_context
            self.current[edge][cat]["_context"] = is_context
        self.current = self.current[edge][cat]
        return self

    def dot(self, req, is_context=False):
        trie = req.trie
        edge = list(trie.keys())[0]
        self.current[edge] = trie[edge]
        if is_context:
            self.current[edge][list(trie[edge].keys())[0]]["_context"] = is_context
            self.current[edge]["_context"] = is_context
        return self


def nodes(cat):
    """Return a query that will look for a token with category cat."""
    return Query(cat=cat)


def edges(edge, cat):
    """Return a query that will look for a dependency arc with label edge
    to a token with category cat.
    """
    return Query(cat=cat, edge=edge)


def find_nodes(query, sentence):
    """Find nodes in sentence that can match query.

    Parameters
    ----------
        sentence : (mweu.cupt.Sentence, mweu.depxml.Sentence)
    """
    def neg_context(trie):
        ret = True
        for val1 in trie.values():
            for val2 in trie.values():
                if val2.get("_context", "") != "-":
                    return False
        return ret

    def find(nodes, trie, what="cat"):
        if what == "cat":
            checkers = [(key, to_checker(key)) for key in trie.keys() if key != "_context"]
            new_nodes = [
                (node, checker[0])
                for node, checker in itertools.product(nodes, checkers)
                if checker[1](node)
            ]
            acc = []
            for node, key in new_nodes:
                the_node = copy.copy(node)
                context = trie[key].get("_context", "")
                if context == "-":
                    return set()
                the_node.context = context
                for result in find([the_node], trie[key], what="edge"):
                    acc.append(set([the_node]) | result)
            return acc
        elif what == "edge":
            new_nodes = []
            for node in nodes:
                acc = set([node])
                for key in [k for k in trie.keys() if k != "_context"]:
                    if neg_context(trie[key]) and not node.outgoing[key]:
                        continue
                    for outg in node.outgoing[key]:
                        results = find([outg], trie[key], what="cat")
                        if not results:
                            return set()
                        for result in results:
                            acc |= result
                    if len(acc) < len(trie[key]):
                        return set()
                new_nodes.append(acc)
            return new_nodes
        else:
            raise ValueError("Cannot request: {}".format(what))
        return set()

    length = len(query)
    result = find(sentence.nodes, query.trie)
    return [item for item in result if len(item) == length]


def to_checker(s):
    """Return a checker based on input.

    Follows the general query syntax where the basic element to query are POS tags.

    Example:
    "noun|lemma=kittens"
    will give something equivalent to:
    lambda x: x.cat == "noun" and x.lemma = "kittens"
    """
    def check(token, filters):
        ok = True
        for key, value in filters:
            try:
                ok = getattr(token, key) == value
            except AttributeError:
                ok = token.features.get(key) == value
            if not ok:
                return False
        return True

    parts = s.split("|")
    if "=" not in parts[0]:
        parts[0] = "cat={}".format(parts[0])
    parts = [part.split("=", 1) for part in parts]
    return functools.partial(check, filters=parts)


# example usage:
# import mweu.depxml as depxml
# import mweu.dpath as dpath
#
#
# req1 = dpath.nodes("v").all_out("object", "nc").all_out("det", "det")
# req2 = dpath.nodes("v")
#       .all_out("object", "nc").dot(edges("N2", "prep")).dot(edges("det", "det"), True)
# req3 = dpath.nodes("v").dot(edges("preparg", "prep")).dot(edges("object", "nc"), True)
# sentence = depxml.read("/home/yoann/corpora/EP/sample/ep_001/0/0/0/ep_001.E6.dis.dep.xml")
#
# print(req1.trie)
# for nodes in dpath.find_nodes(req1, sentence):
#     l = sorted(nodes, key=lambda x:[int(i) for i in x.cluster.split("_")[1:]])
#     print(l)
#     print(" ".join([" ".join(node.fids) for node in l]))
#
# print()
# print(req2.trie)
# for nodes in dpath.find_nodes(req2, sentence):
#     print(sorted(nodes, key=lambda x:[int(i) for i in x.cluster.split("_")[1:]]))
#
# print()
# print(req3.trie)
# for nodes in dpath.find_nodes(req3, sentence):
#     l1 = sorted(nodes, key=lambda x:[int(i) for i in x.cluster.split("_")[1:]])
#     l2 = [node for node in nodes if not node.is_context]
#     print(" ".join([node.lemma for node in l2]))
#     print(" ".join([" ".join(node.fids) for node in l2]))
