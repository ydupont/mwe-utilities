import re


def from_string(tree_str, context_value="context"):
    return to_trie(tree_str.split()[1:-1], context_value=context_value)


def to_trie(tokens, context_value="context"):
    """Transform tokens to a tree structure than can be parsed more easily."""

    context_re = re.compile(r"\|{}=(.+)$".format(context_value))

    def add_indices(tokens):
        lst = []
        i = 0
        for token in tokens:
            if token[0] not in "()":
                lst.append("{token}@{i}".format(token=token, i=i))
                i += 1
            else:
                lst.append(token)
        return lst

    stack = []
    toks = add_indices(tokens)[::-1]
    val = toks.pop()
    val = val[: val.index("@")]
    root_match = context_re.search(val)
    if root_match:
        d = {val[: root_match.start()]: {"_context": root_match.group(1)}}
    else:
        d = {val: {}}
    tmp = [val for val in d.values() if not isinstance(val, str)][0]
    path = [list(d.keys())[0]]
    for i in range(len(toks)):
        item = toks.pop()
        if item.startswith('('):
            item = item[1:]
        stack.append(item)
        if '@' in item:
            cat = stack.pop()
            cat = cat[: cat.index("@")]
            edge = stack.pop()
            key = "({cat},{edge})".format(cat=cat, edge=edge)
            context = context_re.search(cat)
            if context:
                cat = cat[: context.start()]
                tmp[edge] = {cat: {}}
                tmp[edge][cat]["_context"] = context.group(1)
            else:
                tmp[edge] = {cat: {}}
            tmp = tmp[edge][cat]
            path.append(key)
        elif ')' == item:
            path.pop()
            tmp = d
            for node in path:
                try:
                    cat, label = node[1:-1].split(",", 1)
                    context = context_re.search(label)
                    tmp = tmp[label][cat]
                except ValueError:
                    tmp = tmp[node]
    return d


# import mweu.dpath
# import mweu.depxml
#
#
# tokens1 = "(mwe v (object nc (det det ) ) )".split()[1:-1]
# tokens2 = "(mwe v (V::context v (clneg clneg ) (V1 advneg ) ) )".split()[1:-1]
# tokens3 = "(mwe v (preparg prep ) (object::context nc ) )".split()[1:-1]
#
# trie1 = to_trie(tokens1)
# trie2 = to_trie(tokens2)
# trie3 = to_trie(tokens3)
# sentence = mweu.depxml.read("/home/yoann/corpora/EP/sample/ep_001/0/0/0/ep_001.E6.dis.dep.xml")
#
# print()
# query1 = mweu.dpath.Query(trie=trie1, leaf="_")
# for nodes in mweu.dpath.find_nodes(query1, sentence):
#     l = sorted(nodes, key=lambda x:[int(i) for i in x.cluster.split("_")[1:]])
#     print(l)
#     print(" ".join([" ".join(node.fids) for node in l]))
#
# print()
# query2 = mweu.dpath.Query(trie=trie2, leaf="_")
# for nodes in mweu.dpath.find_nodes(query2, sentence):
#     l = sorted(nodes, key=lambda x:[int(i) for i in x.cluster.split("_")[1:]])
#     print(l)
#     print(" ".join([" ".join(node.fids) for node in l]))
#
# print()
# query3 = mweu.dpath.Query(trie=trie3, leaf="_")
# print(query3.trie)
# for nodes in mweu.dpath.find_nodes(query3, sentence):
#     l = sorted(
#         [node for node in nodes if not node.is_context],
#         key=lambda x:[int(i) for i in x.cluster.split("_")[1:]]
#     )
#     print(" ".join([node.lemma for node in l]))
#     print(" ".join([" ".join(node.fids) for node in l]))
