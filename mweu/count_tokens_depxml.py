# -*- coding: utf-8 -*-
"""Count the tokens in a depxml corpus.
"""

import collections
import pathlib
import itertools
import glob
import sys
import multiprocessing
import mweu.depxml as depxml

from lxml import etree


def process(filename):
    """counting stuff in filename"""

    inpath = pathlib.Path(filename)
    try:
        tree = etree.parse(filename)
    except etree.XMLSyntaxError as xml_se:
        print(
            'cannot treat file "{}": {}'.format(
                inpath.name,
                xml_se
            ),
            file=sys.stderr
        )
        return(collections.Counter(), collections.Counter())
    nodes = list(depxml.Node(**node.attrib) for node in tree.iterfind("node"))
    return (
        collections.Counter(node.form for node in nodes if node.form),
        collections.Counter(node.lemma for node in nodes if node.lemma)
    )


def main(infiles, output_file_basename, number_of_processors=1):
    files = list(
        itertools.chain.from_iterable(glob.iglob(infile, recursive=True) for infile in infiles)
    )

    pool = multiprocessing.Pool(processes=number_of_processors)
    results = pool.map(process, files)

    results = list(zip(*results))
    form_counts = sum(results[0], collections.Counter())
    lemma_counts = sum(results[1], collections.Counter())

    with open("{}-form.csv".format(output_file_basename), "w", encoding="utf-8") as output_stream:
        for item, count in form_counts.most_common():
            output_stream.write("{}\t{}\n".format(item, count))
    with open("{}-lemma.csv".format(output_file_basename), "w", encoding="utf-8") as output_stream:
        for item, count in lemma_counts.most_common():
            output_stream.write("{}\t{}\n".format(item, count))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=__doc__.split("\n")[0]
    )
    parser.add_argument(
        "infiles",
        nargs="+",
        help="The files where to look for MWEs."
    )
    parser.add_argument(
        "output_file_basename",
        help="The output file basename."
    )
    parser.add_argument(
        "-p", "--number-of-processors",
        type=int,
        default=1,
        help="The number of processors to use."
    )

    args = parser.parse_args()
    main(**vars(args))
    sys.exit(0)
