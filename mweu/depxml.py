# -*- coding: utf-8 -*-
"""Python objects and methods to handle depxml files.
"""

import collections
import itertools
import re
import tarfile
from lxml import etree


class Cluster:
    """A cluster in a depxml schema"""

    def __init__(
        self, id, left, right, token="", lex="", *args, **kwargs
    ):
        self.id = id
        self.left = int(left)
        self.right = int(right)
        self.token = token
        self.lex = lex
        self.forms = [l for l in self.lex.split() if l]
        # cases like: lex="E____F__|M. X"
        for i in reversed(range(1, len(self.forms))):
            # if '|' not in self.forms[i] or self.forms[i].startswith(r"\|"):
            if not re.search(r"E[0-9]+F[0-9]+\|", self.forms[i]):
                self.forms[i-1] += ' ' + self.forms[i]
                del self.forms[i]
        # file ep_012.E11690.dis.dep.xml:
        # lex="Objde:(de-sn|de-sinf|de-scompl),Objà:(à-sn|à-sinf|à-scompl)&gt;,E11690F22|Super"
        for i, form in enumerate(self.forms):
            if not form.startswith('E'):
                self.forms[i] = form[form.index(",E") + 1:]

    def __len__(self):
        return self.right - self.left


class Node:
    """A Node in a depxml schema"""

    def __init__(
        self, lemma, form, id,
        deriv=None, lemmaid=None, cluster=None, tree=None, cat=None, xcat=None, clusters=None,
        ops=None,
        *args, **kwargs
    ):
        self.lemma = lemma
        self.form = form
        self.id = id
        self.deriv = deriv or ''
        self.lemmaid = lemmaid or ''
        self.cluster = cluster or ''
        self.tree = tree.replace(' ', ';') or ''
        self.cat = cat or ''
        self.xcat = xcat or ''
        self.incoming = collections.defaultdict(list)
        self.outgoing = collections.defaultdict(list)
        if clusters:
            self.lex = clusters[self.cluster]["lex"]
            self.fids = [lex.split("|")[0] for lex in self.lex.split()]
        self.features = {}
        if ops and self.deriv:
            self.features = ops.get(self.deriv, {})

    def __repr__(self):
        return '{}/{}/{}/{}'.format(self.id, self.form, self.lemma, self.cat)


class Edge:
    """An edge in a depxml schema"""

    def __init__(self, id, source, target, type, label, w=None, ws=None):
        self.id = id
        self.source = source
        self.target = target
        self.type = type
        self.label = label
        self.w = w or ''
        self.ws = ws or ''


class NodeList:
    """A list of nodes carrying useful elements to build pathes/trees."""

    def __init__(self, sentence, nodes, items=None):
        self.sentence = sentence
        self.nodes = nodes
        self.pathes = [[node] for node in self.nodes]
        self.pathes1 = {node: {} for node in self.nodes}
        self.leaves = [(self.pathes1, node, node) for node in self.nodes]
        # self.leaves elements ==> (current_node, leaf [key to use], root)
        self.roots = {}
        if items:
            for node in self.nodes:
                self.roots[node] = set(items) - set([node.form, node.lemma])

    def __iter__(self):
        for node in self.nodes:
            yield node

    def incoming(self, label):
        all_nodes = []
        for node in self.nodes:
            all_nodes.extend(node.incoming.get(label, []))
        return NodeList(self.sentence, all_nodes)

    def outgoing2(self, label=None, *filters):
        new_leaves = []
        for path, leaf, root in self.leaves:
            if label:
                lst = leaf.outgoing.get(label, [])
            else:
                lst = list(itertools.chain.from_iterable(leaf.outgoing.values()))
            for fltr in filters:
                lst = [n for n in lst if fltr(n)]
                if not lst:
                    break
            for item in lst:
                path[leaf][item] = {}
                new_leaves.append((path[leaf], item, root))
            self.roots[root] -= set(itertools.chain.from_iterable((x.form, x.lemma) for x in lst))
        self.leaves = new_leaves
        return self


class Sentence:
    """A sentence in a depxml schema."""

    def __init__(self, nodes, edges, name=None):
        self.nodes = nodes
        self.edges = edges
        self.name = name
        self.id2node = {node.id: node for node in self.nodes}
        self.id2edge = {edge.id: edge for edge in self.edges}
        self.tokens = sorted(nodes, key=lambda x: int(x.id[x.id.index("n")+1:]))
        self.nodes2edge = {}
        for edge in edges:
            self.id2node[edge.source].outgoing[edge.label].append(self.id2node[edge.target])
            self.id2node[edge.target].incoming[edge.label].append(self.id2node[edge.source])
            self.nodes2edge[(self.id2node[edge.source], self.id2node[edge.target])] = edge

    def find_nodes(self, items=None, *filters, **kv_filters):
        """Find nodes in the sentence that match specific some filters and create a NodeList
        from those nodes.
        Nodes that should be found can be provided in items kwargument.
        """
        lst = self.nodes
        for k, v in kv_filters.items():
            lst = [node for node in lst if getattr(node, k) == v]
            if not lst:
                break
        for fltr in filters:
            lst = [node for node in lst if fltr(node)]
            if not lst:
                break
        return NodeList(self, lst, items)

    def edge(self, source, target):
        """Return the label of the edge linking source to target."""
        return self.nodes2edge[source, target].label


def find_all(sentence, items, root_category=None):
    """Return the NodeList that could be made looking for items in sentence.

    Calling this method will update the "roots" field in returned NodeList. This
    allows to know, for each root, all the unseen items in items argument. When
    looking for MWEs, we want to find every item in items. The correct root is
    the root where no unseen items are present.
    """
    nodes = sentence.find_nodes(
        items,
        lambda x: set([x.form, x.lemma]) & set(items),
        lambda x: (not root_category) or (x.cat == root_category)
    )
    for i in range(len(items)-1):
        nodes = nodes.outgoing2(
            None,
            lambda x: (not x.form) or set([x.form, x.lemma]) & set(items)
        )
    return nodes


def clean(pathes):
    """Remove pathes of empty elements leading to an empty leaf.

    An element is considered empty if its form is an empty str.
    """
    if not pathes:
        return {}
    new_pathes = {}
    for key in pathes:
        nxt = clean(pathes[key])
        if nxt or key.form:
            new_pathes[key] = nxt
    return new_pathes


def read(filename):
    """Read a file as a Sentence. Throw an exception if file cannot be read."""
    tree = etree.parse(filename)
    return from_data(tree, name=filename)


def from_data(tree, name=None):
    """Create sentence from parsed depxml file."""
    clusters = {cluster.attrib["id"]: cluster.attrib for cluster in tree.iterfind("cluster")}
    ops = {op.attrib["deriv"]: extract_features(op) for op in tree.findall("op")}
    nodes = list(Node(clusters=clusters, ops=ops, **node.attrib) for node in tree.iterfind("node"))
    edges = list(Edge(**edge.attrib) for edge in tree.iterfind("edge"))
    return Sentence(nodes, edges, name=name)


def extract_features(op):
    """Return a dict of morphological features extracted from an op node in depxml tree."""
    sign_map = {
        "minus": "-",
        "plus": "+"
    }
    try:
        features = list(list([narg for narg in list(op) if narg.attrib["type"] == "top"][0])[0])
    except IndexError:  # no features
        return {}
    feats = {}
    for feature in features:
        name = feature.attrib["name"]
        values = [
            (value.text if value.text else value.tag).strip()
            for value in list(feature)
        ]
        if values:
            feats[name] = ";".join([sign_map.get(value, value) for value in values])
    return feats


def read_targz(filename):
    """Return a list of sentences read from the input tar.gz file."""
    sentences = []
    with tarfile.open(filename, "r:gz") as tar:
        members = [m for m in tar.getmembers() if m.name.endswith(".dis.dep.xml")]
        for member in members:
            fd = tar.extractfile(member)
            try:
                sentence = from_data(etree.fromstring(fd.read()), name=member.name)
                sentences.append(sentence)
            except etree.XMLSyntaxError:
                pass
            fd.close()
    return sentences
