"""Transform depXML files into a single treetagger file.
"""

import sys


def main(inputfile, output_file=sys.stdout, column=-2):
    candidates = set()
    i = 0
    for line in inputfile:
        i += 1
        parts = line.strip("\r\n").split("\t")
        candidate = parts[column]
        tokens = candidate.strip().split()
        if ")" in tokens:
            print(i)
        if len(tokens) > 1:
            candidates.add("_".join(tokens))

    for candidate in sorted(candidates):
        output_file.write("{}\n".format(candidate))


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "inputfile",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the pattern file, sys.stdin if not provided."
    )
    parser.add_argument(
        "-o", "--output-file",
        type=argparse.FileType("w"), default=sys.stdout,
        help="The path to the output file (default: standard output)."
    )
    parser.add_argument(
        "-c", "--column",
        type=int,
        default=-2,
        help="The column to read in pattern file (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
